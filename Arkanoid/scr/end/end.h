#ifndef End_H
#define End_H

namespace arkanoid {
	namespace end {

		void initEnd();
		void updateFrameEnd();

	}
}
#endif // End_H
