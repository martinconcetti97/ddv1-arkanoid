#include "end.h"

#include "raylib.h"

#include "screen control/screen_control.h"
#include"gameplay/gameplay.h"
#include "player\player.h"
#include "bullet\bullet.h"
#include "brick/brick.h"

namespace arkanoid {
	namespace end {

		void updateEnd();
		void drawEnd();

		static Vector2 mousePoint;
		static Rectangle rec1;
		static Rectangle rec2;
		Texture2D background;

		void initEnd() {

			background = LoadTexture("../res/assets/images/Background.png");

			rec1.x = static_cast<float>(GetScreenWidth()) / 2 - 50;
			rec1.y = static_cast<float>(GetScreenHeight()) / 2;
			rec1.height = 50;
			rec1.width = 100;

			rec2.x = static_cast<float>(GetScreenWidth()) / 2 - 50;
			rec2.y = static_cast<float>(GetScreenHeight()) / 2 + 60;
			rec2.height = 50;
			rec2.width = 100;

		}
		void updateEnd() {

			mousePoint = GetMousePosition();

			if (CheckCollisionPointRec(mousePoint, rec1)) {

				if (IsMouseButtonPressed(MOUSE_LEFT_BUTTON)) {
					gameplay::gameLevels = gameplay::firstWorld;

					player::initPj();
					bullet::initBullet();
					bricks::initBricks();

					screen_control::Screens = screen_control::Game;

				}
			}

			if (CheckCollisionPointRec(mousePoint, rec2)) {

				if (IsMouseButtonPressed(MOUSE_LEFT_BUTTON)) {
					player::initPj();
					bullet::initBullet();
					bricks::initBricks();
				//	screen_control::endGame = true;
					screen_control::Screens = screen_control::Menu;

				}
			}
		}

		void drawEnd() {

			BeginDrawing();

			ClearBackground(RAYWHITE);

			DrawTexture(background, 0, 0, WHITE);

			DrawRectangle(static_cast<int>(rec1.x), static_cast<int>(rec1.y), static_cast<int>(rec1.width), static_cast<int>(rec1.height), BLACK);
			DrawRectangle(static_cast<int>(rec2.x), static_cast<int>(rec2.y), static_cast<int>(rec2.width), static_cast<int>(rec2.height), BLACK);

			DrawText("Re-jugar", static_cast<int>(rec1.x) + 18, static_cast<int>(rec1.y) + 10, 20, WHITE);
			DrawText("Menu", static_cast<int>(rec2.x) + 18, static_cast<int>(rec2.y) + 10, 20, WHITE);

			DrawText("Arkanoid, por Martin Concetti", 20, 260, 20, BLACK);
			DrawText("Creado usando raylib y sfxr", 130, 280, 20, BLACK);
			DrawText("V1.0", 10, GetScreenHeight() - 20, 20, BLACK);

			EndDrawing();
		}

		void updateFrameEnd() {
			updateEnd();
			drawEnd();
		}
	}
}