#ifndef Credits_H
#define Credits_H

namespace arkanoid {
	namespace credits {

		void initCredits();
		void updateFrameCredits();
	}
}
#endif // Credits_H