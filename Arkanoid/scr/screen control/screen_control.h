#ifndef Screen_control_H
#define Screen_control_H

namespace arkanoid {
	namespace screen_control {

		enum GameScreen
		{
			Menu = 0,
			Game,
			End,
			Credits,
			Controls
		};
		extern GameScreen Screens;
		extern bool endGame;

		extern void InitPlay();
		int game();
	}
}
#endif // Screen_control_H