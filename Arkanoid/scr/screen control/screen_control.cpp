#include "screen control\screen_control.h"

#include "raylib.h"

#include "Menu\menu.h"

#include "Controls\controls.h"
#include "Credits\credits.h"
#include "Gameplay\gameplay.h"
#include "player\player.h"
#include "bullet\bullet.h"
#include "brick/brick.h"
#include "end/end.h"

using namespace arkanoid;

namespace arkanoid {
	namespace screen_control {
		static void change();

		GameScreen Screens;

		bool endGame = false;

		int game() {

			const int screenWidth = 800;
			const int screenHeight = 850;
			InitWindow(screenWidth, screenHeight, "ARKANOID");


			player::initPj();
			bullet::initBullet();
			bricks::initBricks();
			gameplay::initGameplay();
			controls::initControls();
			credits::initCredits();
			menu::initMenu();
			end::initEnd();


			while (!WindowShouldClose()) {
				if (endGame == false) {

					change();
				}
			}
			//gameplay::unload();
			CloseWindow();

			return 0;
		}

		static void change() {

			switch (Screens) {
			case Menu:
				menu::updateMenu();
				break;
			case Game:
				gameplay::updateFrame();
				break;
			case Controls:
				controls::updateFrameControls();
				break;
			case Credits:
				credits::updateFrameCredits();
				break;
			case End:
				end::updateFrameEnd();
			}
		}

	}
}