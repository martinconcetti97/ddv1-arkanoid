#include "raylib.h"

#include "Controls.h"

#include <iostream>

#include "screen control/screen_control.h"

namespace arkanoid {
	namespace controls {

		void updateControls();
		void drawControls();

		Texture2D background;

		static Rectangle rect1;

		Vector2 mause;
		Color mauseColor;		

		bool colorChange = false;
		bool colorChange2 = false;

		bool controlChange = false;
		bool controlChange2 = false;

		void initControls() {

			background = LoadTexture("../res/assets/images/Background.png");

			rect1.x = 25;
			rect1.y = 25;
			rect1.height = 50;
			rect1.width = 100;
		}

		void updateControls() {


			if (CheckCollisionPointRec(mause, rect1)) {

				if (IsMouseButtonPressed(MOUSE_LEFT_BUTTON)) {

					screen_control::Screens = screen_control::Menu;
				}
			}

			if (CheckCollisionPointRec(mause, rect1)) {

				if (IsMouseButtonPressed(MOUSE_LEFT_BUTTON)) {

					screen_control::Screens = screen_control::Menu;

				}
			}
		}

		void drawControls() {

			BeginDrawing();
			ClearBackground(RAYWHITE);

			DrawTexture(background, 0, 0, WHITE);

		
			DrawRectangle(static_cast<int>(rect1.x), static_cast<int>(rect1.y), static_cast<int>(rect1.width), static_cast<int>(rect1.height), BLACK);
		
			ClearBackground(RAYWHITE);
			DrawText("atras", static_cast<int>(rect1.x) + 18, static_cast<int>(rect1.y) + 10, 20, WHITE);
			DrawText("P para pausar la partida", 40, 120, 20, BLACK);
			DrawText("Espacio para disparar", 40, 160, 20, BLACK);
			DrawText("Flechas para moverse ", 40, 180, 20, BLACK);


			EndDrawing();
		}

		void updateFrameControls() {
			updateControls();
			drawControls();
		}
	}
}