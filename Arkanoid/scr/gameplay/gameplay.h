#ifndef Gameplay_H
#define Gameplay_H

namespace arkanoid {
	namespace gameplay {

		enum LEVELS {
			firstWorld,
			secondWorld,
			thirdWorld,
		};

		void initGameplay();
		void updateFrame();

		extern bool endGame;

		extern LEVELS gameLevels;

	}
}
#endif // Gameplay_H