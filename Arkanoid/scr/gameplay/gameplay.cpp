#include "gameplay.h"

#include <iostream>
#include "raylib.h"

#include "screen control/screen_control.h"
#include "player/player.h"
#include "bullet/bullet.h"
#include "brick/brick.h"

namespace arkanoid {
	namespace gameplay {

		static void updateInput();
		static void updateGameplay();

		static void drawGameplay();

		static Vector2 mousePoint;
		static Rectangle rec1;
		LEVELS gameLevels;

		bool reInit1 = false;
		bool reInit2 = false;

		bool pause = false;
		Texture2D background;

		void initGameplay() {
			gameLevels = firstWorld;

		}
		void updateInput() {
			if (IsKeyPressed('P')) pause = !pause;


			if (pause == false) {

				player::inputPj();
			}
		}

		void updateGameplay() {

			if (pause == false) {
				
				switch (gameLevels) {
				case arkanoid::gameplay::firstWorld:
					bricks::updateBricks();
					player::updatePj();
					bullet::updateBullet(player::pj.body, player::pj.onStartBullet);

					break;
				case arkanoid::gameplay::secondWorld:
				/*	if (reInit1 == false) {
						player::initPj();
						bullet::initBullet();
						bricks::initBricks();
						reInit1 = true;
					}*/
					player::updatePj();
					bullet::updateBullet(player::pj.body, player::pj.onStartBullet);
					bricks::updateBricks();
					break;
				case arkanoid::gameplay::thirdWorld:
					if (reInit2 == false) {
						player::initPj();
						bullet::initBullet();
						bricks::initBricks();
						std::cout << bricks::restbricksPerLvl << std::endl;
						reInit2 = true;
					}
					player::updatePj();
					bullet::updateBullet(player::pj.body, player::pj.onStartBullet);
					bricks::updateBricks();
					break;
				}
			}
		}

		void drawGameplay() {

			BeginDrawing();

			if (pause == false) {

				ClearBackground(RAYWHITE);
				player::drawPj();
				bullet::drawBullet(bullet::bullet);
				bricks::drawBricks();
				//DrawText("POINTS", static_cast<int>(GetScreenHeight()) + 18, static_cast<int>(GetScreenWidth()) - 40, 20, WHITE);
				DrawText(TextFormat("Lives: %02i", player::points),static_cast<int>(GetScreenHeight()) + 18, static_cast<int>(GetScreenWidth()) - 40, 20, BLUE);

			}

			DrawFPS(10, 5);
			EndDrawing();
		}

		void updateFrame() {
			updateInput();
			updateGameplay();
			drawGameplay();
		}
	}
}