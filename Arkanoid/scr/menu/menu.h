#ifndef Menu_H
#define Menu_H

namespace arkanoid {
	namespace menu {


		void initMenu();
		void updateMenu();
		void drawMenu();
	}
}
#endif // Menu_H