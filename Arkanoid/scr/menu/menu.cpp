#include "Menu.h"

#include<iostream>

#include "raylib.h"

#include "screen control/screen_control.h"

namespace arkanoid {
	namespace menu {


		Texture2D background2;

		static Vector2 mousePoint;
		static Rectangle rec1;
		static Rectangle rec2;
		static Rectangle rec3;


		void initMenu() {

			background2 = LoadTexture("res/Play.png");

			rec1.x = static_cast<float>(GetScreenWidth()) / 2 - 50;
			rec1.y = static_cast<float>(GetScreenHeight()) / 2 ;
			rec1.height = 50;
			rec1.width = 100;

			rec2.x = static_cast<float>(GetScreenWidth()) / 2 - 50;
			rec2.y =  static_cast<float>(GetScreenHeight()) / 2 + 60;
			rec2.height = 50;
			rec2.width = 100;

			rec3.x = static_cast<float>(GetScreenWidth()) / 2 - 50;
			rec3.y = static_cast<float>(GetScreenHeight()) / 2 + 120;
			rec3.height = 50;
			rec3.width = 100;

		}
		void updateMenu() {

			drawMenu();
			mousePoint = GetMousePosition();

			if (CheckCollisionPointRec(mousePoint, rec1)) {

				if (IsMouseButtonPressed(MOUSE_LEFT_BUTTON)) {

					screen_control::Screens = screen_control::Game;
				}
			}
			if (CheckCollisionPointRec(mousePoint, rec2)) {

				if (IsMouseButtonPressed(MOUSE_LEFT_BUTTON)) {

					screen_control::Screens = screen_control::Credits;

				}
			}
			if (CheckCollisionPointRec(mousePoint, rec3)) {

				if (IsMouseButtonPressed(MOUSE_LEFT_BUTTON)) {

					screen_control::Screens = screen_control::Controls;
				}
			}
		}

		void drawMenu() {

			ClearBackground(RAYWHITE);

			DrawTexture(background2, rec1.x, rec1.y, WHITE);

			DrawRectangle(static_cast<int>(rec2.x), static_cast<int>(rec2.y), static_cast<int>(rec2.width), static_cast<int>(rec2.height), BLACK);
			DrawRectangle(static_cast<int>(rec3.x), static_cast<int>(rec3.y), static_cast<int>(rec3.width), static_cast<int>(rec3.height), BLACK);

			DrawText("Creditos", static_cast<int>(rec2.x) + 5, static_cast<int>(rec2.y) + 10, 18, WHITE);
			DrawText("Controles", static_cast<int>(rec3.x) + 5, static_cast<int>(rec3.y) + 10, 18, WHITE);
			DrawText("Arkanoid", GetScreenWidth() / 2 - 110, 50, 100, MAROON);

			EndDrawing();
		}

	}
}