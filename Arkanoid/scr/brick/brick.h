#ifndef BRICKS_H
#define BRICKS_H

#include "raylib.h"

const int BRISCKS_PER_LINE = 10;
const int BRICKS_LINES = 6;

namespace arkanoid {
	namespace bricks {

		struct BRICKS {
			Rectangle brick;
			bool destroyed;
		};

		extern BRICKS bricks2[BRICKS_LINES][BRISCKS_PER_LINE];
		extern int restbricksPerLvl;

		void initBricks();
		void drawBricks();
		void updateBricks();
		void deinitBricks();
	}
}
#endif // !BRICKS_H