#include "brick.h"

#include <iostream>

#include "Bullet/bullet.h"
#include"gameplay/gameplay.h"

namespace arkanoid {
	namespace bricks {

		int restbricksPerLvl = 0;
		BRICKS bricks2[BRICKS_LINES][BRISCKS_PER_LINE];
		static Color brickcolors[12] = { RED,GREEN,BLACK,PURPLE,DARKGREEN,YELLOW,VIOLET,ORANGE,BLUE,DARKBLUE,BROWN,PINK };
		static float speedDown;
		static float WidthBrick;
		static float HeightBrick;

		void initBricks() {
			
			switch (gameplay::gameLevels) {
			case gameplay::firstWorld:
				speedDown = 7.5f;
				restbricksPerLvl = 5;
				break;
			case gameplay::secondWorld:
				speedDown = 5.5f;
				restbricksPerLvl = 2;
				break;
			case gameplay::thirdWorld:
				speedDown = 2.5f;
				restbricksPerLvl = 0;
				break;
			}
			WidthBrick = GetScreenWidth() / BRISCKS_PER_LINE;
			HeightBrick = 50.0f;

			for (int i = 0; i < BRICKS_LINES - restbricksPerLvl; i++) {
				for (int j = 0; j < BRISCKS_PER_LINE; j++) {
					//---------------------	WIDTH
					bricks2[i][j].brick.width = WidthBrick;
					//---------------------	HEIGHT
					bricks2[i][j].brick.height = HeightBrick;
					//---------------------	POS X
					bricks2[i][j].brick.x = (j * bricks2[i][j].brick.width + bricks2[i][j].brick.width / BRISCKS_PER_LINE);
					//---------------------	POS Y
					bricks2[i][j].brick.y = (i * bricks2[i][j].brick.height + bricks2[i][j].brick.height);
					//--------------------------------
					bricks2[i][j].destroyed = false;
					//--------------------------------
				}
			}
		}
		void drawBricks() {

			for (int i = 0; i < BRICKS_LINES; i++) {
				for (int j = 0; j < BRISCKS_PER_LINE; j++) {
					if ((i + j) % 2 == 0) {
						DrawRectangleRec(bricks2[i][j].brick, brickcolors[j]);
						DrawRectangleLinesEx(bricks2[i][j].brick, 2, BLACK);
						DrawRectangleLinesEx(bricks2[i][j].brick, 1, GREEN);
					}
					else {
						DrawRectangleRec(bricks2[i][j].brick, brickcolors[i]);
						DrawRectangleLinesEx(bricks2[i][j].brick, 2, BLACK);
						DrawRectangleLinesEx(bricks2[i][j].brick, 1, GREEN);
					}
				}
			}
		}
		void updateBricks() {
			for (int i = 0; i < BRICKS_LINES - restbricksPerLvl; i++) {
				for (int j = 0; j < BRISCKS_PER_LINE; j++) {
					if (bricks2[i][j].destroyed) {
						bricks2[i][j].brick.x = 1800;
					}
					else
						bricks2[i][j].brick.y += speedDown * GetFrameTime();
				}
			}
		}
		void deinitBricks() {
			bricks2[BRICKS_LINES][BRISCKS_PER_LINE] = { 0 };
		}
	}
}