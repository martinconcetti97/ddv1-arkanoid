#include "bullet.h"

#include <iostream>

#include "player/player.h"
#include "brick/brick.h"


namespace arkanoid {
	namespace bullet {

		static void checkBulletImpacts();
		static void increaseSpeed();
		static void bulletPhysichs();
		static void calcImpactPoints();
		static void playerImpact();
		
		static int impactExit;
		static int impactExitBrick;
		
		BULLET bullet;
		float POSX;
		float POSY;
		float SPEEDX;
		float SPEEDY;
		
		static float growSpeed;
		static bool capSpeed;

		
		void initBullet() {
			
			impactExit = 0;
			
			growSpeed = 30.0f;
			capSpeed = false;
			
			SPEEDX = 0.0f;
			SPEEDY = 400.0f;
			bullet.RADIUS = 10.0f;
			POSX = static_cast<float>(GetScreenWidth() / 2 - bullet.RADIUS / 2);
			POSY = static_cast<float>(GetScreenHeight() - (bullet.RADIUS + 60));
			bullet.POS = { POSX,POSY };
			bullet.SPEED = { SPEEDX , SPEEDY };
			bullet.penetration = 2.5f;
#if DEBUG
			std::cout << "RADIO INIT:" << bullet::bullet.RADIUS << std::endl;
#endif // DEBUG
		}
		void drawBullet(BULLET& bala) {
			DrawCircleV(bala.POS, bala.RADIUS, BLACK);
#if DEBUG
			DrawCircleLines(static_cast<int>(bala.pointTop.x), static_cast<int>(bala.pointTop.y), bala.RADIUS, GREEN);
			DrawCircleLines(static_cast<int>(bala.pointBot.x), static_cast<int>(bala.pointBot.y), bala.RADIUS, GREEN);
			DrawCircleLines(static_cast<int>(bala.pointLeft.x), static_cast<int>(bala.pointLeft.y), bala.RADIUS, GREEN);
			DrawCircleLines(static_cast<int>(bala.pointRight.x), static_cast<int>(bala.pointRight.y), bala.RADIUS, GREEN);
#endif // DEBUG
		}
		void checkBulletImpacts() {
			if (bullet.POS.x - bullet.RADIUS < 0 || bullet.POS.x + bullet.RADIUS > GetScreenWidth()) { bullet.SPEED.x *= -1.0f; impactExit = 0; }
			else if (bullet.POS.y - bullet.RADIUS < 0) { bullet.SPEED.y *= -1.0f; impactExit = 0; }
			if (bullet.POS.y - bullet.RADIUS >	GetScreenHeight()) {
				player::pj.onStartBullet = true;
				bullet.SPEED = { SPEEDX , SPEEDY };
				player::pjLifes--;
			}
			for (int i = 0; i < BRICKS_LINES - bricks::restbricksPerLvl; i++) {
				for (int j = 0; j < BRISCKS_PER_LINE; j++) {
					if (CheckCollisionPointRec(bullet.pointTop, bricks::bricks2[i][j].brick)
						|| CheckCollisionPointRec(bullet.pointBot, bricks::bricks2[i][j].brick)
						&& impactExitBrick != 1) {

						player::pjBricksBroke--;
#if DEBUG
						std::cout << player::pjBricksBroke << std::endl;
#endif // DEBUG

						bullet.SPEED.y *= -1.0f;
						impactExitBrick = 1;
						impactExit = 0;
						increaseSpeed();
						bricks::bricks2[i][j].destroyed = true;
					}
					if (CheckCollisionPointRec(bullet.pointLeft, bricks::bricks2[i][j].brick)
						|| CheckCollisionPointRec(bullet.pointRight, bricks::bricks2[i][j].brick)
						&& impactExitBrick != 2) {

						player::pjBricksBroke--;
#if DEBUG
						std::cout << player::pjBricksBroke << std::endl;
#endif // DEBUG

						bullet.SPEED.x *= -1.0f;
						impactExitBrick = 2;
						impactExit = 0;
						increaseSpeed();
						bricks::bricks2[i][j].destroyed = true;
					}
				}
			}
			calcImpactPoints();
		}
		void increaseSpeed() {
			
			if (!capSpeed) {
				if (bullet.SPEED.y > 0) bullet.SPEED.y += growSpeed;
				else bullet.SPEED.y -= growSpeed;
				
				if (bullet.SPEED.x > 0) bullet.SPEED.x += growSpeed;
				else bullet.SPEED.x -= growSpeed;
			}
			
			if ((bullet.SPEED.y > 600.0f || -bullet.SPEED.y < -600.0f) && (bullet.SPEED.x > 800.0f || -bullet.SPEED.x < -800.0f))
				capSpeed = true;
			else
				capSpeed = false;
		}
		
		void updateBullet(Rectangle& player, bool onStart) {
			if (onStart)
				bullet.POS = { (player.x + (player.width / 2)),player.y - (bullet.RADIUS + 30) };
			else
				bulletPhysichs();
		}
		
		void bulletPhysichs() {
			
			checkBulletImpacts();
			
			bullet.POS.x -= bullet.SPEED.x * GetFrameTime();
			bullet.POS.y -= bullet.SPEED.y * GetFrameTime();
			
			playerImpact();
			
		}
		
		void calcImpactPoints() {
			
			bullet.pointTop.x = bullet.SPEED.x * GetFrameTime();
			bullet.pointTop.y -= bullet.SPEED.y * GetFrameTime();
			bullet.pointTop = { bullet.POS.x,bullet.POS.y - (bullet.RADIUS + 4) };
			
			bullet.pointBot.x = bullet.SPEED.x * GetFrameTime();
			bullet.pointBot.y -= bullet.SPEED.y * GetFrameTime();
			bullet.pointBot = { bullet.POS.x,bullet.POS.y + (bullet.RADIUS + 4) };
			
			bullet.pointLeft.x = bullet.SPEED.x * GetFrameTime();
			bullet.pointLeft.y -= bullet.SPEED.y * GetFrameTime();
			bullet.pointLeft = { bullet.POS.x - (bullet.RADIUS + 4) ,bullet.POS.y };
			
			bullet.pointRight.x = bullet.SPEED.x * GetFrameTime();
			bullet.pointRight.y -= bullet.SPEED.y * GetFrameTime();
			bullet.pointRight = { bullet.POS.x + (bullet.RADIUS + 4) ,bullet.POS.y };
			
		}
		void playerImpact() {
			if (CheckCollisionPointRec(bullet.pointBot, player::pj.divisons[2]) && impactExit != 1) {
				bullet.SPEED.y *= -1.0f;
				bullet.SPEED.x = 0.0f;
				impactExit = 1;
			}
			else if (CheckCollisionPointRec(bullet.pointBot, player::pj.divisons[1]) && impactExit != 2) {
				bullet.SPEED.y *= -1.0f;
				bullet.SPEED.x = (player::pj.SPEED / 4);
				impactExit = 2;
			}
			else if (CheckCollisionPointRec(bullet.pointBot, player::pj.divisons[0]) && impactExit != 3) {
				bullet.SPEED.y *= -1.0f;
				bullet.SPEED.x = (player::pj.SPEED / 2);
				impactExit = 3;
			}
			else if (CheckCollisionPointRec(bullet.pointBot, player::pj.divisons[3]) && impactExit != 4) {
				bullet.SPEED.y *= -1.0f;
				bullet.SPEED.x = (player::pj.SPEED / -4);
				impactExit = 4;
			}
			else if (CheckCollisionPointRec(bullet.pointBot, player::pj.divisons[4]) && impactExit != 5) {
				bullet.SPEED.y *= -1.0f;
				bullet.SPEED.x = (player::pj.SPEED / -2);
				impactExit = 5;
			}
			
		}
	}
}
