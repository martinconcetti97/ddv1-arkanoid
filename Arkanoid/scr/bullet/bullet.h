#ifndef BULLET_H
#define BULLET_H

#include "raylib.h"

namespace arkanoid {
	namespace bullet {


		struct BULLET {
			Vector2 POS;
			float RADIUS;
			Vector2 SPEED;
			float penetration;

			Vector2 pointTop;
			Vector2 pointBot;
			Vector2 pointLeft;
			Vector2 pointRight;
		};

		extern BULLET bullet;

		void initBullet();
		void drawBullet(BULLET& bullet);
		void updateBullet(Rectangle& player, bool onStart);

	}
}
#endif // !BULLET_H
