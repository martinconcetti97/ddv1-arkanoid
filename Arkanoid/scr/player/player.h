#ifndef PLAYER_H
#define PLAYER_H

#include "raylib.h"

namespace arkanoid {
	namespace player {

		const int DIVAMOUNT = 5;

		struct PLAYER {
			Rectangle body;
			Rectangle divisons[DIVAMOUNT];
			Rectangle lifes[5];
			bool leftSide;
			bool rightSide;
			float SPEED;

			bool playerAlive;
			bool playerMoving;
			bool onStartBullet;
			bool forceLeft;
			bool forceRight;
		};

		extern PLAYER pj;
		extern int pjLifes;
		extern int pjBricksBroke;
		extern int points;

		void initPj();
		void inputPj();
		void drawPj();
		void updatePj();
		void checkLimit();
		void deinitPj();
	}
}
#endif // !PLAYER_H
