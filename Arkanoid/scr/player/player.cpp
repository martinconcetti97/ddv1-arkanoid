#include "player.h"

#include <iostream>

#include "raylib.h"

#include "screen control/screen_control.h"
#include "brick/brick.h"
#include "gameplay/gameplay.h"

namespace arkanoid {
	namespace player {

		PLAYER pj;
		static float POSX;
		static float POSY;
		static float WIDTH;
		static float HEIGHT;
		static float WIDTHDIV;

		int pjLifes;
		int pjBricksBroke;

		int points = 0;

		void initPj() {

			//Todo se rompe, dios no existe y toca harcodear

			switch (gameplay::gameLevels) {
			case gameplay::firstWorld:
				pjBricksBroke = 10;
				break;
			case gameplay::secondWorld:
				pjBricksBroke = 40;

				break;
			case gameplay::thirdWorld:
				pjBricksBroke = 60;
				break;
			}
			
			
			WIDTH = 200.0f;
			HEIGHT = 20.0f;
			WIDTHDIV = WIDTH / 5;
			POSX = static_cast<float>(GetScreenWidth() / 2 - WIDTH / 2);
			POSY = static_cast<float>(GetScreenHeight() - (HEIGHT + 20));
			
			pj.onStartBullet = true;
			pj.forceLeft = false;
			pj.forceRight = false;
			pj.playerMoving = false;
			pj.SPEED = 500.0f;
			pjLifes = 5;
			pj.playerAlive = true;

			for (int i = 2; i < pjLifes + 2; i++) {
				pj.lifes[i - 2].width = 10;
				pj.lifes[i - 2].height = 50;
				pj.lifes[i - 2].x = 10;
				pj.lifes[i - 2].y = static_cast<float>(GetScreenHeight() - (60 * i));
			}

			for (int i = 0; i < DIVAMOUNT; i++) {
				if (i == 0)
					pj.divisons[i] = { POSX,POSY,WIDTHDIV,HEIGHT };
				else
					pj.divisons[i] = { POSX + (WIDTHDIV * i) ,POSY,WIDTHDIV,HEIGHT };
			}

			pj.body = { POSX,POSY,WIDTH,HEIGHT };
		}
		
		void inputPj() {
			


			if (IsKeyDown(KEY_RIGHT) && !pj.rightSide) {
				pj.body.x += pj.SPEED * GetFrameTime();
				for (int i = 0; i < DIVAMOUNT; i++) {
					pj.divisons[i].x += pj.SPEED * GetFrameTime();
				}
				pj.forceRight = true;
				pj.forceLeft = false;
				pj.playerMoving = true;
			}
			else { pj.forceRight = false; pj.playerMoving = false; }
			
			if (IsKeyDown(KEY_LEFT) && !pj.leftSide) {
				pj.body.x -= pj.SPEED * GetFrameTime();
				for (int i = 0; i < DIVAMOUNT; i++) {
					pj.divisons[i].x -= pj.SPEED * GetFrameTime();
				}
				pj.forceRight = false; pj.forceLeft = true;
				pj.playerMoving = false;
			}
			else { pj.forceLeft = false; pj.playerMoving = false; }
			
			if (IsKeyPressed(KEY_SPACE)) { pj.onStartBullet = false; }
			
			if (IsKeyReleased(KEY_R))	pj.onStartBullet = true;
			
		}
		
		void drawPj() {
			
			DrawRectangleRec(pj.body, GOLD);
			
#if DEBUG
			for (int i = 0; i < DIVAMOUNT; i++) { DrawRectangleLinesEx(pj.divisons[i], 1, GREEN); }
#endif 
			
			for (int i = 0; i < pjLifes; i++) {
				DrawRectangleRec(pj.lifes[i], VIOLET); DrawRectangleLinesEx(pj.lifes[i], 2, BLACK);
			}
			
		}
		
		void updatePj() {
			
			checkLimit();

			if (pjLifes < 1) { pj.playerAlive = false; }
			
			if (!pj.playerAlive && pjBricksBroke > 0) {
				screen_control::Screens = screen_control::End;

			}
			else if (pj.playerAlive && pjBricksBroke <= 0) {

				switch (gameplay::gameLevels) {
				case gameplay::firstWorld:
					gameplay::gameLevels = gameplay::secondWorld;
					std::cout << "salis del primero" << std::endl;
				case gameplay::secondWorld:
					std::cout << "salis del segundo" << std::endl;

					gameplay::gameLevels = gameplay::thirdWorld;
					break;
				case gameplay::thirdWorld:
					std::cout << "fin" << std::endl;

					screen_control::Screens = screen_control::End;
					break;
				}
			}
		}
		
		void checkLimit() {
			
			if (pj.body.x < 0)
				pj.leftSide = true;
			else
				pj.leftSide = false;
			
			if (pj.body.x > GetScreenWidth() - WIDTH)
				pj.rightSide = true;
			else
				pj.rightSide = false;
			
		}
		
		void deinitPj() {}
		
	}
}
